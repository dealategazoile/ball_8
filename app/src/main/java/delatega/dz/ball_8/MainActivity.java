package delatega.dz.ball_8;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonSa9si = (Button) findViewById(R.id.button_sa9si);

        final ImageView imageResultat= (ImageView) findViewById(R.id.imageView_resultat);


        final int[] lesChoix = {
                R.drawable.ball1,
                R.drawable.ball2,
                R.drawable.ball3,
                R.drawable.ball4,
                R.drawable.ball5
        };
         buttonSa9si.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Random random = new Random();

                 int number = random.nextInt(4);

                 imageResultat.setImageResource(lesChoix[number]);

             }
         });

    }
}